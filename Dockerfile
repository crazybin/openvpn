FROM alpine:latest
RUN apk upgrade --no-cache
RUN apk add --no-cache s6-overlay openvpn
COPY /root /
EXPOSE 1194/udp
ENTRYPOINT ["/init"]
