openssl req -x509 -newkey ed25519 -keyout server.key -out server.crt -nodes -sha256 -days 3650 -subj '/CN=server'
openssl x509 -fingerprint -sha256 -in server.crt -noout
openssl req -x509 -newkey ed25519 -keyout client.key -out client.crt -nodes -sha256 -days 3650 -subj '/CN=client'
openssl x509 -fingerprint -sha256 -in client.crt -noout
